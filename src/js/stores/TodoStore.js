import { EventEmitter } from "events";

class TodoStore extends EventEmitter {
	constructor() {
		super()
		this.todos = [
			{
				id: 1232323,
				text: "Go shopping",
				complete: false
			},
			{
				id: 2123453,
				text: "Book ticket",
				complete: false
			},
		];
	}

	getAll() {
		return this.todos; 
	}
}

const todoStore = new TodoStore;
export default todoStore;